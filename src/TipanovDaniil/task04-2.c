#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	srand(time(0));
	char str[20][256];
	int i, N = 0;
	printf("Enter N: ");
	scanf("%d", &N);
	if (N > 20 || N < 1)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	puts("Enter strings:");
	for (i = 0; i <= N; i++)
	{
		fgets(str[i], 256, stdin);
		if (i!= 0 && str[i][0] == '\n')
			break;
	}
	printf("\n");
	i = rand() % (N - 1 + 1) + 1;
	puts(str[i]);
	return 0;
}