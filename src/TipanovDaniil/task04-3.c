#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>

void sortS(int strs[][2], int amount)
{
	int i, j, temp;
	for (i = 1; i <= amount - 1; i++)
		for (j = amount; j > i; j--)
			if (strs[j][2] < strs[j - 1][2])
			{
				temp = strs[j-1][1];
				strs[j - 1][1] = strs[j][1];
				strs[j][1] = temp;
				temp = strs[j - 1][2];
				strs[j - 1][2] = strs[j][2];
				strs[j][2] = temp;
			}
}

int main()
{
	char str[20][256];
	int sortStr[20][2]; //sortStr[i][1] - ����� i ������, sortStr[i][2] - ����� i ������
	int i, N = 0, amount = 0;
	printf("Enter N: ");
	scanf("%d", &N);
	if (N > 20 || N < 1)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	puts("Enter strings:");
	for (i = 0; i <= N; i++)
	{
		fgets(str[i], 256, stdin);
		if (i != 0 && str[i][0] == '\n') //��������, ������� � 1-�� �������� (��. ������� ����������), ��� ��� � ������� ������� ��������� ������ �������� �� ����� ������ (������ ���?)
			break;
		sortStr[i][1] = i;
		sortStr[i][2] = strlen(str[i]);
	}
	amount = i -1;
	printf("\n");
	sortS(sortStr, amount);
	for (i = 1; i <= amount; i++)
		printf("%s", str[sortStr[i][1]]);
	return 0;
}