#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
	srand(time(0));
	char str[20][256];
	int i, N = 0;
	int randS[20];
	short int flag = 0;
	printf("Enter N: ");
	scanf("%d", &N);
	if (N > 20 || N < 1)
	{
		printf("INPUT ERROR!\n");
		return 1;
	}
	puts("Enter strings:");
	for (i = 0; i <= N; i++)
	{
		fgets(str[i], 256, stdin);
		if (i != 0 && str[i][0] == '\n')
		{
			N = i-1;
			break;
		}
	}
	printf("\n");
	for (i = 0; i <= N; i++)
	{
		randS[i] = rand() % (N - 0 + 1) + 0;
		for (int j = 0; j < i; j++)
		{
			if (randS[i] == randS[j])
			{
				i--;
				flag = 0;
				break;
			}
			else
				flag = 1;
		}
		if (flag)
			puts(str[randS[i]]);
	}
	return 0;
}